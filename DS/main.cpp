#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cmath>

using namespace std;

void Isort(int *A, int n);
void Isort(int64_t *A, int n);
void Rsort(int *A, int n);
void Rsort(int64_t *A, int n);
int64_t assemble(int *A);
void split(int64_t large,int A[6]);
int Isearch(int64_t *A, int64_t x, int n);
int Rsearch(int64_t *A, int64_t x, int left, int right);
void print(int64_t large, int pos);

int main(void){
    
    char select;
    int lottery[999][6],num_lot,hand[9999][6],num_hand;
    int64_t biglot[999],bighand[9999];
    
    while (cin>>select){
        
        cin>>num_lot;
        for (int i=0;i<num_lot;i++){                  //Read in lottery numbers
            for (int j=0;j<6;j++)
                cin>>lottery[i][j];
        }
        
        switch (select){                              //Selecting algorithm
            case 'I':
                for (int i=0;i<num_lot;i++){
                    Isort(lottery[i],6);              //Horizontal sort first
                    biglot[i]=assemble(lottery[i]);   //Assemble into a big number
                }
                Isort(biglot,num_lot);                //Vertical sort
                for (int i=0;i<num_lot;i++){
                    split(biglot[i],lottery[i]);      //Separate and then overwrite lottery[i][j]
                }break;
            case 'R':
                for (int i=0;i<num_lot;i++){
                    Rsort(lottery[i],6);
                    biglot[i]=assemble(lottery[i]);
                }
                Rsort(biglot,num_lot);
                for (int i=0;i<num_lot;i++){
                    split(biglot[i],lottery[i]);
                }break;
            case 'S':
                for (int i=0;i<num_lot;i++){
                    std::sort(lottery[i],lottery[i]+6);
                    biglot[i]=assemble(lottery[i]);
                }
                std::sort(biglot,biglot+num_lot);
                for (int i=0;i<num_lot;i++){
                    split(biglot[i],lottery[i]);
                }break;
        }                                             //End of switch
        
        cout<<select<<endl;
        cout<<num_lot<<endl;
        
        for (int i=0;i<num_lot;i++){
            for (int j=0;j<5;j++){
                if (lottery[i][j]<10)
                    cout<<"0"<<lottery[i][j]<<" ";
                else
                    cout<<lottery[i][j]<<" ";
            }
            if (lottery[i][5]<10)
                cout<<"0"<<lottery[i][5]<<endl;
            else
                cout<<lottery[i][5]<<endl;
        }
        
        cin>>num_hand;
        
        for (int i=0;i<num_hand;i++){
            for (int j=0;j<6;j++)
                cin>>hand[i][j];
        }
        
        cout<<num_hand<<endl;
        
        switch (select){                              //Start of switch
            case 'I':
                for (int i=0;i<num_hand;i++){
                    Isort(hand[i],6);
                    bighand[i]=assemble(hand[i]);
                }
                for (int i=0;i<num_hand;i++){
                    int p=Isearch(biglot,bighand[i],num_lot);
                    print(bighand[i],p);
                    if (i!=num_hand-1)
                        cout<<endl;
                }break;
            case 'R':
                for (int i=0;i<num_hand;i++){
                    Rsort(hand[i],6);
                    bighand[i]=assemble(hand[i]);
                }
                for (int i=0;i<num_hand;i++){
                    int p=Rsearch(biglot,bighand[i],0,(num_lot-1));
                    print(bighand[i],p);
                    if (i!=num_hand-1)
                        cout<<endl;
                }break;
            case 'S':
                /*for (int i=0; i<number_of_givenlottery; i++)
                    sort(givenlottery[i], givenlottery[i]+6);
                
                for (int i=0; i<number_of_givenlottery; i++)
                {
                    bignummber[i]= givenlottery[i][0]*10000000000;
                    bignummber[i]+=givenlottery[i][1]*100000000;
                    bignummber[i]+=givenlottery[i][2]*1000000;
                    bignummber[i]+=givenlottery[i][3]*10000;
                    bignummber[i]+=givenlottery[i][4]*100;
                    bignummber[i]+=givenlottery[i][5]*1;
                }
                sort(bignummber,bignummber+number_of_givenlottery);
                
                                //cout<<number_of_buylottery<<endl;
                for (int i=0; i<number_of_buylottery; i++)
                    sort(buylottery[i],buylottery[i]+6);
                
                for (int i=0; i<number_of_buylottery; i++)
                {
                    buy_bignummber[i]= buylottery[i][0]*10000000000;
                    buy_bignummber[i]+=buylottery[i][1]*100000000;
                    buy_bignummber[i]+=buylottery[i][2]*1000000;
                    buy_bignummber[i]+=buylottery[i][3]*10000;
                    buy_bignummber[i]+=buylottery[i][4]*100;
                    buy_bignummber[i]+=buylottery[i][5]*1;
                }
                

                    
                    int64_t *pos=find (bignummber, bignummber+number_of_givenlottery,buy_bignummber[i] );

                    
                    
                    
                    
                }*/
                for (int i=0;i<num_hand;i++){
                    std::sort(hand[i],hand[i]+6);
                    bighand[i]=assemble(hand[i]);
                }
                for (int i=0;i<num_hand;i++){
                    int64_t *ppt=std::find(biglot,biglot+num_lot,bighand[i]);
                    int64_t index=ppt-biglot+1;
                    if (index==num_lot+1)               //Correction of std::find
                        index=-1;
                    print(bighand[i],index);
                    if (i!=num_hand-1)
                        cout<<endl;
                }break;
        }                                             //End of switch
    }                                                 //End of while
    return 0;
}                                                     //End of main function

void Isort(int *A,int n){
    
    for (int i=0;i<n;i++){
        int j=i;
        for (int k=i+1;k<n;k++)
            if (A[k]<A[j]) j=k;
        std::swap(A[i],A[j]);
    }
}

void Isort(int64_t *A,int n){
    
    for (int i=0;i<n;i++){
        int j=i;
        for (int k=i+1;k<n;k++)
            if (A[k]<A[j]) j=k;
        std::swap(A[i],A[j]);
    }
}

void Rsort(int *A, int n){
    
    if (n==0) return;
    
    for (int i=0;i<n-1;i++){
        if (A[i+1]<A[i])
            swap(A[i],A[i+1]);
    }
    Rsort(A,n-1);
}

void Rsort(int64_t *A,int n){
    
    if (n==0) return;
    
    for (int i=0;i<n-1;i++){
        if (A[i+1]<A[i])
            swap(A[i],A[i+1]);
    }
    Rsort(A,n-1);
}

int64_t assemble(int *A){
    
    int64_t bignummber=0;
    
    bignummber+=A[0]*pow(10,10);
    bignummber+=A[1]*pow(10,8);
    bignummber+=A[2]*pow(10,6);
    bignummber+=A[3]*pow(10,4);
    bignummber+=A[4]*pow(10,2);
    bignummber+=A[5]*pow(10,0);
    
    return bignummber;
}

void split(int64_t bignummber,int A[6]){
    
    A[0]=bignummber/10000000000;
    A[1]=(bignummber/100000000)%100;
    A[2]=(bignummber/1000000)%100;
    A[3]=(bignummber/10000)%100;
    A[4]=(bignummber/100)%100;
    A[5]=bignummber%100;
}

int Isearch(int64_t *A, int64_t x, int n){
    
    int left=0,right=n-1,middle;
    
    while (left<=right){
        middle=(right+left)/2;
        if (x<A[middle]) right=middle-1;
        else if (x>A[middle]) left=middle+1;
        else return middle+1;
    }
    return -1;
}

int Rsearch(int64_t *A,int64_t x,int left,int right){
    
    int middle;
    
    if (left<=right){
        middle=(right+left)/2;
        if (x<A[middle]) return Rsearch(A,x,left,middle-1);
        else if (x>A[middle]) return Rsearch(A,x,middle+1,right);
        else return middle+1;
    }
    return -1;
}

void print(int64_t bignummber,int pos){
    
    int printout[6];
    
    printout[0]=bignummber/10000000000;
    printout[1]=(bignummber/100000000)%100;
    printout[2]=(bignummber/1000000)%100;
    printout[3]=(bignummber/10000)%100;
    printout[4]=(bignummber/100)%100;
    printout[5]=bignummber%100;
    
    for (int i=0;i<6;i++){
        if (printout[i]<10)
            cout<<"0"<<printout[i]<<" ";
        else cout<<printout[i]<<" ";
    }
    if (pos==-1)
        cout<<"N";
    else cout<<"Y "<<pos;
}
